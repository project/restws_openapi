<?php

/**
 * Implements hook_restws_openapi_alter().
 *
 * Alter the OpenAPI array before converting to JSON.
 */
function hook_restws_openapi_alter(&$openapi) {

}

/**
 * Implements hook_restws_openapi_resources_alter().
 *
 * Alter the resources to include in the OpenAPI documentation.
 */
function hook_restws_openapi_resources_alter(&$resources) {

}
