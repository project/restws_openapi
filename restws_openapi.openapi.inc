<?php

/**
 * Return JSON in OpenAPI 3.0 format.
 */
function restws_openapi_openapi() {
  $resources = restws_get_resource_info();
  $components = [];
  $components['securitySchemes']['basic'] = [
    'type' => 'http',
    'scheme' => 'basic',
  ];
  $paths = [];

  $allowed = variable_get('restws_openapi_resources') ?? [];
  drupal_alter('restws_openapi_resources', $allowed);

  $resources = array_intersect_key($resources, array_combine($allowed, $allowed));

  ksort($resources);

  foreach ($resources as $resource_path => $resource) {

    $tags[] = [
      'name' => $resource['label'],
      'description' => t('Manage @label entities.', ['@label' => $resource['label']]),
    ];

    $controller = restws_resource_controller($resource_path);

    foreach ($controller->propertyInfo() as $property => $property_info) {
      if (!empty($property_info['computed'])) {
        continue;
      }

      $openApiType = $openApiFormat = '';

      if (empty($property_info['type']) && !empty($property_info['schema field'])) {
        $openApiType = 'string';
      }
      elseif (empty($property_info['type'])) {
        // Maybe this isn't a field? example "URL"
        $openApiType = 'string';
      }
      elseif (in_array($property_info['type'], ['integer'])) {
        $openApiType = 'integer';
      }
      elseif (in_array($property_info['type'], ['positive_integer'])) {
        $openApiType = 'integer';
      }
      elseif (in_array($property_info['type'], ['decimal'])) {
        $openApiType = 'integer';
        $openApiFormat = 'float';
      }
      elseif (in_array($property_info['type'], ['boolean'])) {
        $openApiType = 'boolean';
      }
      elseif (in_array($property_info['type'], [
        'string',
        'text_formatted',
        'text',
      ])) {
        $openApiType = 'string';
      }
      elseif ($property_info['type'] == 'token') {
        $openApiType = 'string';
      }
      elseif (entity_get_info($property_info['type'])) {
        // This is a reference to an entity.
        $openApiType = 'integer';
      }
      elseif ($property_info['type'] == 'entity') {
        // This is a reference to an entity.
        $openApiType = 'integer';
      }
      elseif ($property_info['type'] == 'date') {
        // Ugh, handle this mess.
        $openApiType = 'string';
        $openApiFormat = 'date-time';
      }
      elseif ($property_info['type'] == 'duration') {
        // Ugh, handle this mess.
        $openApiType = 'integer';
      }
      elseif ($property_info['type'] == 'struct') {
        //$openApiType = 'object';
        continue;
      }
      elseif (strpos($property_info['type'], '<') !== FALSE) {
        // Example: list<text>
        //$openApiType = 'array';
        continue;
      }
      else {
        continue;
      }

      // We do not care about feeds.
      if (strpos($property, 'feeds_') === 0) {
        continue;
      }
      if (strpos($property, 'feed_') === 0) {
        continue;
      }
      if ($property == 'vid') {
        continue;
      }

      $components['schemas'][$resource_path]['properties'][$property]['type'] = $openApiType;
      $components['schemas'][$resource_path]['properties'][$property]['description'] = $property_info['description'] ?? '';
    }

    $paths['/' . $resource_path . '.json'] = [
      'get' => [
        'description' => t('Get a list of @label entities.', ['@label' => $resource['label']]),
        'tags' => [$resource['label']],
        'summary' => t('Get a list of @label entities.', ['@label' => $resource['label']]),
        'responses' => [
          '200' => [
            'description' => t('Returns a list of @label.', ['@label' => $resource['label']]),
            'content' => [
              'application/json' => [
                'schema' => [
                  'type' => 'object',
                  'properties' => [
                    'self' => [
                      'type' => 'string',
                      'format' => 'uri',
                      'description' => t('Service URI to the current page'),
                    ],
                    'first' => [
                      'type' => 'string',
                      'format' => 'uri',
                      'description' => t('Service URI to the first page'),
                    ],
                    'next' => [
                      'type' => 'string',
                      'format' => 'uri',
                      'description' => t('Service URI to the next page'),
                    ],
                    'last' => [
                      'type' => 'string',
                      'format' => 'uri',
                      'description' => t('URI to the last page'),
                    ],
                    'list' => [
                      'type' => 'array',
                      'items' => [
                        '$ref' => '#/components/schemas/' . $resource_path,
                      ],
                    ],
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ];

    $paths['/' . $resource_path . "/{{$resource_path}}.json"] = [
      'get' => [
        'tags' => [$resource['label']],
        'summary' => t('Get a single @label.', ['@label' => $resource['label']]),
        'parameters' => [
          [
            'in' => 'path',
            'name' => $resource_path,
            'schema' => ['type' => 'integer'],
            'required' => TRUE,
            'description' => t('Numeric ID of the @label to get.', ['@label' => $resource['label']]),
          ],
        ],
        'responses' => [
          '200' => [
            'content' => [
              'application/json' => [
                'schema' => [
                  '$ref' => '#/components/schemas/' . $resource_path,
                ],
              ],
            ],
            'description' => t('Get a @label.', ['@label' => $resource['label']]),
          ],
        ],
      ],
    ];
    $paths['/' . $resource_path] = [
      'post' => [
        'tags' => [$resource['label']],
        'summary' => t('Create a new @label.', ['@label' => $resource['label']]),
        'requestBody' => [
          'required' => TRUE,
          'content' => [
            'application/json' => [
              'schema' => [
                '$ref' => '#/components/schemas/' . $resource_path,
              ],
            ],
          ],
        ],
        'responses' => [
          '201' => [
            'description' => t('@label created successfully.', ['@label' => $resource['label']]),
            'content' => [
              'application/json' => [
                'schema' => [
                  '$ref' => '#/components/schemas/created_response',
                ],
              ],
            ],
          ],
          '4XX' => [
            'description' => t('Client error creating @label - check format.', ['@label' => $resource['label']]),
          ],
        ],
      ],
    ];

    $paths['/' . $resource_path . "/{{$resource_path}}"] = [
      'put' => [
        'tags' => [$resource['label']],
        'summary' => t('Update an existing @label.', ['@label' => $resource['label']]),
        'description' => 'Update an existing @label by numeric ID.',
        'parameters' => [
          [
            'in' => 'path',
            'name' => $resource_path,
            'schema' => ['type' => 'integer'],
            'required' => TRUE,
            'description' => t('Numeric ID of the @label to update.', ['@label' => $resource['label']]),
          ],
        ],
        'responses' => [
          '200' => [
            'description' => t('The updated @label.', ['@label' => $resource['label']]),
          ],
          '4XX' => [
            'description' => t('Client error updating @label - check format.', ['@label' => $resource['label']]),
          ],
        ],
      ],
      'delete' => [
        'tags' => [$resource['label']],
        'summary' => t('Delete a single @label.', ['@label' => $resource['label']]),
        'parameters' => [
          [
            'in' => 'path',
            'name' => $resource_path,
            'schema' => ['type' => 'integer'],
            'required' => TRUE,
            'description' => t('Numeric ID of the @label to delete.', ['@label' => $resource['label']]),
          ],
        ],
        'responses' => [
          '200' => [
            'description' => t('@label deleted successfully.', ['@label' => $resource['label']]),
          ],
        ],
      ],
    ];
  }

  $components['schemas']['created_response']['properties']['uri']['type'] = 'string';
  $components['schemas']['created_response']['properties']['id']['type'] = 'integer';
  $components['schemas']['created_response']['properties']['resource']['type'] = 'string';


  $json = [
    'openapi' => '3.0.0',
    'security' => [
      ['basic' => []],
    ],
    'info' => [
      'title' => variable_get('site_name'),
      'description' => t('OpenAPI 3.0 specification.'),
      'version' => '1.0.0',
    ],
    'servers' => [
      [
        'url' => rtrim(url('/', ['absolute' => TRUE]), '/'),
      ],
    ],
    'paths' => $paths,
    'components' => $components,
    'tags' => $tags,
  ];

  drupal_alter('restws_openapi', $json);

  header('Content-type: application/json');
  print json_encode($json, JSON_PRETTY_PRINT);
  exit;
}
